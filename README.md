# Project Management
Mangement project for the Halium 10 / Android 10 development

## Getting started
In case you are starting a new device, please come over to our porting channel for initial instructions until we have a detailed documentation. Halium 10 is not ready for production yet, so bear with us until all main features are working and free of bugs. However devices should be able to get boot, have WiFi and network, camera, sensors, GPS etc.

## Roadmap

### Currently being worked on

1. Halium 10 recovery and installer integration (@amartinz)

### Next Up
1. Finish sensors API
1. Finish lights API

## Support


## Contributing
